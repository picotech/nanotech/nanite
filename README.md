# nanite

***

## TODO



***

## Links



***

## Tags

[![](tags/project/nanite.svg)](https://github.com/shadowrylander/nanite)

| Platform | Type | Language | Modules Used |
| - | - | - | - |
| ![](tags/platform/all.svg) | ![](tags/type/module.svg) | [![](tags/language/python3.6.0.svg)](https://www.python.org/downloads/release/python-360/) | [![](tags/modules_used/collections.svg)](https://docs.python.org/3/library/collections.html) |
|  |  |  | [![](tags/modules_used/itertools.svg)](https://docs.python.org/3/library/itertools.html) |
|  |  |  | [![](tags/modules_used/os.svg)](https://docs.python.org/3/library/os.html) |
|  |  |  | [![](tags/modules_used/typing.svg)](https://docs.python.org/3/library/typing.html) |

***

## Notes

*

***

## Content

A small but important component of the nanotech module.

***

## Saku

<!-- saku start -->

### install

    python3 -m pip install .

<!-- saku end -->

***

## Copyright
