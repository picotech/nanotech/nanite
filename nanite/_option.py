# Imports
import click

# From Imports
from loguru import logger
from sty import fg
from sys import exit
from typing import Dict, List, Any, Callable
from webcolors import hex_to_rgb

colors: Dict[str, Callable[..., Any]] = {
	"red": fg.red,
	"pink": fg(*hex_to_rgb("#f69")),
}
colorize = (
	lambda color, string: f"{colors[color]}{string}{fg.rs}"
)

class Error(Exception):
	pass


class illegal_usage(Error):
	pass


class _option:
	class Option(click.Option):
		"""
			Answer: https://stackoverflow.com/questions/55874075/use-several-options-together-or-not-at-all
			User: https://stackoverflow.com/users/7311767/stephen-rauch

			"xor"        : list of options this can't be used with
			"one_req"    : list of options of which one or more must be used
			"all_req"    : same as required
			"req_one_of" : list of options of which one or more must be used with this option
			"req_all_of" : list of options of which all must be used with this option

			Simply put the entire list of options in the keyword argument value; the help text and usage error will correct itself accordingly.
		"""

		def __init__(self, *args, **kwargs):
			self.xor: List[str] = kwargs.pop("xor", [])
			self.one_req: List[str] = kwargs.pop("one_req", [])
			self.req_one_of: List[str] = kwargs.pop("req_one_of", [])
			self.req_all_of: List[str] = kwargs.pop("req_all_of", [])
			help: str = kwargs.get("help", "")

			if "-" not in args[0] or "--" not in args[0]:
				name = args[0]
			elif "-" in args[0]:
				if "--" in args[1]:
					name = args[1].partition("--")[2]
				else:
					name = args[1]
			elif "--" in args[0]:
				name = args[0].partition("--")[2]

			if self.xor:
				self.xor_joined = (
					self.xor[0]
					if len(self.xor) == 1
					else '", "'.join(
						option
						for option in self.xor
						if option != name
					)
				)

				kwargs[
					"help"
				]: str = f'{help}\nNOTE: This option is mutually exclusive with {"option" if len(self.xor) == 1 else "options"} ["{self.xor_joined}"].'

				help: str = kwargs.get("help", "")

			if self.one_req:
				self.one_req_joined = (
					self.one_req[0]
					if len(self.one_req) == 1
					else '", "'.join(
						option
						for option in self.one_req
						if option != name
					)
				)

				kwargs[
					"help"
				]: str = f'{help}\nNOTE: This option must be used if {"option" if len(self.one_req) == 1 else "options"} ["{self.one_req_joined}"] {"is" if len(self.one_req) == 1 else "are"} not.'
				help: str = kwargs.get("help", "")

			if self.req_one_of:
				self.req_one_of_joined = (
					self.req_one_of[0]
					if len(self.req_one_of) == 1
					else '", "'.join(
						option
						for option in self.req_one_of
						if option != name
					)
				)

				kwargs[
					"help"
				]: str = f'{help}\nNOTE: This option requires {"the use" if len(self.req_one_of) == 1 else "one or more"} of {"option" if len(self.req_one_of) == 1 else "options"} ["{self.req_one_of_joined}"] as well.'

				help: str = kwargs.get("help", "")

			if self.req_all_of:
				self.req_all_of_joined = (
					self.req_all_of[0]
					if len(self.req_all_of) == 1
					else '", "'.join(
						option
						for option in self.req_all_of
						if option != name
					)
				)

				kwargs[
					"help"
				]: str = f'{help}\nNOTE: This option requires the use of {"option" if len(self.req_all_of) == 1 else "options"} ["{self.req_all_of_joined}"] as well.'

				help: str = kwargs.get("help", "")

			super().__init__(*args, **kwargs)

		def handle_parse_result(self, ctx, opts, args):
			"""

				Note to self: "self.name in opts" was being used because if it wasn't,
				the if condition would match regardless of whether this option is being used or not;
				so for example, if option "a" is mutually exclusive to option "b", and
				"self.name in opts" wasn't used, "command -a -b" would match the condition, but so would
				"command -a" and "command -b", given that the option is still being parsed by
				the program.
			"""

			if (
				self.name in opts
				and self.xor
				and any(option in opts for option in self.xor)
			):
				logger.error(
					colorize("red", "Sorry, something happened!\n\n")
				)
				raise illegal_usage(
					f'Illegal Usage: "{self.name}" is mutually exclusive with {"option" if len(self.xor) == 1 else "options"}:\n\n["{self.xor_joined}"]'
				)

			if (
				self.one_req
				and self.name not in opts
				and not any(
					option in opts for option in self.one_req
				)
			):
				logger.error(
					colorize("red", "Sorry, something happened!\n\n")
				)
				raise illegal_usage(
					f'Illegal Usage: one of these options are required:\n\n["{self.one_req_joined}"]'
				)

			if (
				self.name in opts
				and self.req_one_of
				and not any(
					option in opts for option in self.req_one_of
				)
			):
				logger.error(
					colorize("red", "Sorry, something happened!\n\n")
				)
				raise illegal_usage(
					f'Illegal Usage: "{self.name}" must be used with {"" if len(self.req_one_of) == 1 else "one or more of"} {"option" if len(self.req_one_of) == 1 else "options"}:\n\n["{self.req_one_of_joined}"]'
				)

			if (
				self.name in opts
				and self.req_all_of
				and not all(
					option in opts for option in self.req_all_of
				)
			):
				logger.error(
					colorize("red", "Sorry, something happened!\n\n")
				)
				raise illegal_usage(
					f'Illegal Usage: "{self.name}" must be used with {"option" if len(self.req_all_of) == 1 else "options"}:\n\n["{self.req_all_of_joined}"]'
				)

			return super().handle_parse_result(
				ctx, opts, args
			)